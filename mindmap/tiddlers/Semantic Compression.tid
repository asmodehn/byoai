created: 20191216111034942
modified: 20191216140910773
tags: 
title: Semantic Compression
type: text/vnd.tiddlywiki

In an Unbounded Energy World, Information transformation is almost free.
However one must still consider computational space-time scales, and various ways of interacting with the world.

As intelligent beings, like animals we sense the world, derive meaning, and store the information perceived in memory. In the same way that we don't store the entirety of the information, and that we might forget things that we never recall, a "practical" AI is bounded in information content, and must therefore "compress" the information received and still derive meaning from it.
TODO : references (Schmidthuber, ... )

In a discrete space time context, we can observe a change in state as atomic, and start from there.
An event describe the time when that change is *observed* (remember we have an 'external' topdown functional point of view here, to have formal semantics of things that are outside the scope of our programming language), along with the new state.
e = (t, s)

A set of these events can happen during some time interval. Note the change is the "trigger" of the event, however the only thing we can observe (without requiring memory) is the new state.
Change can be rebuilt from a pair of state, but it already require some memory in space (storage) or time (some kind of recursion), and therefore will not be considered atomic.

Another set of events e' = (t', s') can be observed, and a relation can be hypothesized:

If t = t' + dt and s' = F(s), with dt small time interval, with F functional relation (no observed counter example)  Then ==>> e' = F(e). There is a relation F between e' and e. Often, if dt is relatively small for the outer level time scale we care about, it can be omitted for clarity (we lose time information here - often neglected in computers, unless 'realtime' is specifically required.) 

A function is therefore one kind of "semantic compression" of a set of events. Instead of having to store two set of correlated events, it is sufficient to store the set of events on domain(F) and a representation of F, deducted from the set of observed events e'.
Note that no information is lost here => we have derived some "meaning", ie, we found an abstraction.

Lossy compression can be justifiably done when:
 - we need to store less event (internal space bounds requirements)
 - we need to simplify the states observed : s' = s + ds, where ds is deemed "small enough" for the outer space scale we care about.

The Problem then becomes how do we represent events and functions in a unified way. See Knowledge Representation for more details on this.

References:

To put this into perspective, Scott Wlaschin book about Functional Domain Modeling and his talk about 13 ways of looking at a turtle are quite inspirational.


